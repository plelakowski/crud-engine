package pl.cm.crud.repository;

import org.springframework.data.repository.CrudRepository;

import pl.cm.crud.model.User;

public interface UserRepository extends CrudRepository<User,Long> {
}
