package pl.cm.crud.model;

public final class UserBuilder {
  private Long id;
  private String username;
  private String firstname;
  private String lastname;
  private Integer age;

  private UserBuilder() {
  }

  public static UserBuilder anUser() {
    return new UserBuilder();
  }

  public UserBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public UserBuilder withUsername(String username) {
    this.username = username;
    return this;
  }

  public UserBuilder withFirstname(String firstname) {
    this.firstname = firstname;
    return this;
  }

  public UserBuilder withLastname(String lastname) {
    this.lastname = lastname;
    return this;
  }

  public UserBuilder withAge(Integer age) {
    this.age = age;
    return this;
  }

  public User build() {
    User user = new User();
    user.setId(id);
    user.setUsername(username);
    user.setFirstname(firstname);
    user.setLastname(lastname);
    user.setAge(age);
    return user;
  }
}
