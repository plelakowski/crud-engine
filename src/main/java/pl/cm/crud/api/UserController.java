package pl.cm.crud.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.cm.crud.model.User;
import pl.cm.crud.repository.UserRepository;

@RestController
@RequestMapping("/api")
public class UserController {

  private final UserRepository userRepository;

  @Autowired
  public UserController(final UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @PutMapping("user/{userId}")
  public void update(final @RequestBody User user, @PathVariable("userId") final Long userId){
    final User userFromDb = userRepository.findOne(userId);
    userFromDb.setAge(user.getAge());
    userFromDb.setFirstname(user.getFirstname());
    userFromDb.setLastname(user.getLastname());
    userFromDb.setUsername(user.getUsername());
    userRepository.save(userFromDb);
  }

}
